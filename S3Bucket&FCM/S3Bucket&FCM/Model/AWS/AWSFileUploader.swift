//
//  AWSFileUploader.swift
//
//
//  Created by Anisha on 19/11/17.
//  Copyright © 2017 Anisha. All rights reserved.
//

import UIKit
import AWSS3

class AWSFileUploader: NSObject {
    //Please add the Access Key ahd Secret Key Before Use
    struct AWS {
        static let awsAccessKey = "IAIP5LUHZONCIQC2UQ"
        static let awsSecretKey = "ks4lKaQq1iGPBYVNyRDAIxIlH4k/FOodiNcWF"
        static let bucketName   = "tethers"
        static let uploadPath   = "https://s3-ap-southeast-2.amazonaws.com/"
    }
    
    func uploadImageToAWS(image: UIImage?, fileUrl: URL?, type: fileTypes, uploadFinish:@escaping (_ status:Bool, _ imgPath:String) -> Void) -> Void
    {
        let time = Date()
        let df = DateFormatter()
        df.dateFormat = "dd_MM_yyyy_hh_mm_ss"
        let timeString = df.string(from: time)
        var fileName: String
        var contantType: String
        var savedFilePath: URL
        switch type {
        case fileTypes.Video:
            fileName = "File_\(timeString).mov"
            contantType = "movie/mov"
            savedFilePath = fileUrl!
        case fileTypes.File:
            let extsion = fileUrl!.pathExtension
            fileName = "File_\(timeString)." + extsion
            contantType = "binary/octet-stream"
            savedFilePath = fileUrl!
        default:
            fileName = "File_\(timeString).jpg"
            contantType = "image/jpg"
            savedFilePath = saveImage(image: image!, fileName: fileName)
            break
        }
        
    //Create Upload Request For File Uploading
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.bucket = AWS.bucketName as String
        uploadRequest?.key = fileName
        uploadRequest?.acl = AWSS3ObjectCannedACL(rawValue: 2)!
        uploadRequest?.body = savedFilePath
        uploadRequest?.contentType = contantType
        uploadRequest?.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
            DispatchQueue.main.async(execute: {
                let amountUploaded = totalBytesSent // To show the updating data status in label.
                print(amountUploaded)
            })
        }
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (awsTask:AWSTask<AnyObject>) -> Any? in
            if ((awsTask.error) != nil) {
                print("Error in uploading image")
                uploadFinish(false,"Error in uploading image")
            }
            else
            {
                var uploadOutput = AWSS3TransferManagerUploadOutput()
                uploadOutput = awsTask.result as! AWSS3TransferManagerUploadOutput?
                print("result :\(String(describing: uploadOutput))")
                let fileManager = FileManager.default
                if fileManager.fileExists(atPath: savedFilePath.absoluteString) {
                    do{
                        try fileManager.removeItem(atPath: savedFilePath.absoluteString)
                    }
                    catch{}
                }
                uploadFinish(true,(AWS.uploadPath+fileName) as String)
            }
            return nil
        }
    )}
    func saveImage(image: UIImage, fileName: String) -> URL{
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let savedImagePath = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(fileName)
        // imageView is my image from camera
        let imageData = UIImagePNGRepresentation(image)
        
        do {
            try  imageData?.write(to: savedImagePath, options: .withoutOverwriting)
        }
        catch {
            print("Error While Saving imag in document directory")
        }
        return savedImagePath
    }
    
    func uploadVideoeToAWS(fileUrl: URL, uploadFinish:@escaping (_ status:Bool, _ filePath:String) -> Void) -> Void
    {
        let time = Date()
        let df = DateFormatter()
        df.dateFormat = "dd_MM_yyyy_hh_mm_ss"
        let timeString = df.string(from: time)
        let fileName = "File_\(timeString).mov"
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = fileUrl as URL
        uploadRequest?.key = fileName
        uploadRequest?.bucket = AWS.bucketName as String
        uploadRequest?.acl = AWSS3ObjectCannedACL.publicRead
        uploadRequest?.contentType = "movie/mov"
        
        uploadRequest?.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
            DispatchQueue.main.async(execute: {
                let amountUploaded = totalBytesSent // To show the updating data status in label.
                print(amountUploaded)
            })
        }
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task) in
            if task.error != nil {
                print(task.error.debugDescription)
                uploadFinish(false,"Error in uploading image")
            } else {
                // Do something with your result.
                print("done")
                uploadFinish(true,(AWS.uploadPath+fileName) as String)
            }
            return nil
        })

    }
}




