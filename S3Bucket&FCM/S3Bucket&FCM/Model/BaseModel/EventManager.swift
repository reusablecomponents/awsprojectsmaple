//
//  EventManager.swift
//  Tow Assist
//
//  Created by Signity on 23/05/17.
//  Copyright © 2017 signity solution. All rights reserved.
//

import UIKit
import MBProgressHUD

// swiftlint:disable line_length
// swiftlint:disable trailing_whitespace
typealias ActionHandler = ((String?) -> Void)?
class EventManager: NSObject {
   
// MARK: -  Show Native Alert 
    class func showAlert(alertMessage: String?, delegate: UIViewController) {
        let alert=UIAlertController(title: ConstantModal.ProjectName, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: ConstantModal.btnOk, style: UIAlertActionStyle.default, handler: nil))
        delegate.present(alert, animated: true, completion: nil)
    }
// MARK: -  Show Native Alert with custom Actions 
    class func showAlertWithAction(alertMessage: String?, btn1Tit: String?, btn2Tit: String?, sender: UIViewController, action: ActionHandler) {
        let alert = UIAlertController(title: ConstantModal.ProjectName, message: alertMessage, preferredStyle: .alert)
        if btn1Tit != nil {
            alert.addAction(UIAlertAction(title: btn1Tit, style: UIAlertActionStyle.default, handler: {_ in
                action?(btn1Tit)
            }))
        }
        if btn2Tit != nil {
            alert.addAction(UIAlertAction(title: btn2Tit, style: UIAlertActionStyle.default, handler: {_ in
                action?(btn2Tit)
            }))
        }
        sender.present(alert, animated: true, completion: nil)
    }
//// MARK: -  Check for Internet 
//    class func checkForInternetConnection() -> Bool {
//        return ((Reachability()?.connection)! == .none) ? false : true
//    }

// MARK: -  Loader 
    class func showloader() {
        MBProgressHUD.showAdded(to: ConstantModal.appDelegate.window!, animated: true)
    }
    
    class func hideloader() {
        MBProgressHUD.hide(for: ConstantModal.appDelegate.window!, animated: true)
    }

}
