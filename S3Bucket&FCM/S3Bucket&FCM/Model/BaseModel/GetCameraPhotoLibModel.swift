//
//  GetCameraPhotoLibModel.swift
//  Tow Assist
//
//  Created by Signity on 12/02/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import MobileCoreServices
var selectedImageHandler : ((UIImage?) -> Void)?
var selectedVideoHandler : ((NSURL?) -> Void)?
var selectedFileHandler : ((NSURL?) -> Void)?
extension UIImage {
    func scaleAndRotate() -> UIImage {
        let destinationSize = CGSize(width: 600, height: 600)
        UIGraphicsBeginImageContext(destinationSize)
        self.draw(in: CGRect(x: 0, y: 0, width: destinationSize.width, height: destinationSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

class GetCameraPhotoLibModel: NSObject, UIImagePickerControllerDelegate, UIDocumentPickerDelegate, UINavigationControllerDelegate {
    
    static let shared = GetCameraPhotoLibModel()
    // MARK: -  Check Image Size 
    class func checkImageSize(image:UIImage!) -> Bool{
        let imgData: NSData = NSData(data: UIImagePNGRepresentation(image)!)
        return self.checkFileSize(fileData: imgData)
    }
    // MARK: -  Check Document Size 
    class func checkDocumentSize(file:NSURL!) -> Bool{
        var fileData: NSData?
        do {
            fileData = try NSData(contentsOf: file as URL, options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            fileData = nil
        }
        return fileData != nil ? self.checkFileSize(fileData: fileData!) : false
    }
    class func checkFileSize(fileData: NSData) -> Bool{
        let fileSize: Int = fileData.length
        let sizeinKB = Double(fileSize) / 1024.0
        return sizeinKB > 8000 ? false : true
    }
      // MARK: -  get Camera/Photos alert controller 
     func getCameraPhotos(picker: UIImagePickerController, sender: UIViewController)  {
        picker.delegate = self
        let actionSheetController: UIAlertController = UIAlertController(title: ConstantModal.btnSelect, message: "", preferredStyle: .actionSheet)
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModal.btnCancel, style: .cancel, handler: nil))
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModal.btnTakePhoto, style: .default, handler: { action in
           self.openCamera(picker: picker, sender: sender)
        }))
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModal.btnSelectPhoto, style: .default, handler: { action in
            self.openphotoLibrary(picker: picker, sender: sender, mediaType: [kUTTypeImage as String])
            
        }))
        sender.present(actionSheetController, animated: true, completion: nil)
    }
     // MARK: -  get Attachement alert controller 
    func getAttachementFile(imagePicker: UIImagePickerController,filePicker: UIDocumentPickerViewController, sender: UIViewController)  {
        imagePicker.delegate = self
        filePicker.delegate = self
        let actionSheetController: UIAlertController = UIAlertController(title: ConstantModal.btnSelect, message: "", preferredStyle: .actionSheet)
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModal.btnCancel, style: .cancel, handler: nil))
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModal.btnSelectFile, style: .default, handler: { action in
            self.openDocumentFolder(picker: filePicker, sender: sender)
        }))
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModal.btnSelectMedia, style: .default, handler: { action in
            self.openphotoLibrary(picker: imagePicker, sender: sender, mediaType: [kUTTypeMovie as String, kUTTypeImage as String])
            
        }))
        sender.present(actionSheetController, animated: true, completion: nil)
    }
    
    //MARK: -  Open Camera For User Profile Image 
     func openCamera(picker: UIImagePickerController, sender: UIViewController) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.allowsEditing = true
            picker.modalPresentationStyle = .fullScreen
            sender.present(picker,animated: true,completion: nil)
        }
    }
    func openphotoLibrary(picker: UIImagePickerController, sender: UIViewController, mediaType: [String]) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            picker.allowsEditing = true
            picker.modalPresentationStyle = .fullScreen
            picker.mediaTypes = mediaType // Add media type
            sender.present(picker,animated: true,completion: nil)
        }
    }
    //MARK: -  Open Document Picker For User Profile Image 
    func openDocumentFolder(picker: UIDocumentPickerViewController, sender: UIViewController) {
        sender.present(picker, animated: true, completion: nil)
    }
    // MARK: -  UIImagePicker Delegates 
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let mediaType:AnyObject? = info[UIImagePickerControllerMediaType] as AnyObject
        if let type:AnyObject = mediaType {
            if type is String {
                let stringType = type as! String
                if stringType == kUTTypeMovie as String {
                    let urlOfVideo = info[UIImagePickerControllerMediaURL] as? NSURL
                    GetCameraPhotoLibModel.checkDocumentSize(file: urlOfVideo) ? selectedVideoHandler!(urlOfVideo) : GetCameraPhotoLibModel.showErrorMessage(picker, controller: nil, message: ConstantModal.videoSize)
                    print(urlOfVideo?.pathExtension ?? "No Path")
                    print("-----------------------------------")
                    print(urlOfVideo?.lastPathComponent ?? "No Componenet")
                } else if stringType == kUTTypeImage as String {
                    var  chosenImage = UIImage()
                    chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage //2
                    let userProfileImage = chosenImage.scaleAndRotate()
                    GetCameraPhotoLibModel.checkImageSize(image: userProfileImage) ? selectedImageHandler!(userProfileImage) : GetCameraPhotoLibModel.showErrorMessage(picker, controller: nil, message: ConstantModal.ImageSize)
                }
            }
          }
        }
   
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    // MARK: -  UIDocumentPicker Delegates 
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL){
        let urlOfFile = url as NSURL
        print(url.pathExtension )
        print("-----------------------------------")
        print(url.lastPathComponent )
        selectedFileHandler!(urlOfFile)
//        GetCameraPhotoLibModel.checkDocumentSize(file: urlOfFile) ? selectedFileHandler!(urlOfFile) : GetCameraPhotoLibModel.showErrorMessage(nil, controller: controller, message: ConstantModal.videoSize)
        }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
        
        }
    // MARK: -  Error message if size of Image/Video/Document is more then 10MB 
    class func showErrorMessage(_ picker: UIImagePickerController?, controller: UIDocumentPickerViewController?, message:String) {
        
        picker != nil ? picker?.dismiss(animated: true, completion: nil) : nil
        controller != nil ? controller?.dismiss(animated: true, completion: nil) : nil
        EventManager.showAlertWithAction(alertMessage: message, btn1Tit: ConstantModal.btnOk, btn2Tit: nil, sender: (ConstantModal.appDelegate.window?.rootViewController)!, action: nil)
    }
}
