//
//  ConstantModal.swift
//  Tow Assist
//

import UIKit
// swiftlint:disable line_length
// swiftlint:disable trailing_whitespace
class ConstantModal: NSObject {
    
    static let ProjectName         = "S3 Bucket& FCM"
    static let appDelegate         = (UIApplication.shared.delegate as? AppDelegate)!
    static let rootNavController   = ConstantModal.appDelegate.window?.rootViewController as? UINavigationController
   
    static let ServiceFailure      = "Something went wrong, please try again."
    static let WeakInternet        = "Please check your internet connection before using."
    static let NoInternet          = "No internet connection."
    static let FillAllField        = "Please fill in all required fields."
    static let FillAllAddressField = "Address, City and Postal Code are required fields."
    static let EnterEmail          = "Please enter email address!"
    static let EnterPassword       = "Please enter password!"
    static let ConfirmPassword     = "Please confirm password!"
    static let ValidEmail          = "Please enter Valid email."
    static let EnterName           = "Please enter name!"
    static let EnterMobile         = "Please enter phone number!"
    static let EnterSubject        = "Please enter Subject!"
    static let EnterMessage        = "Please enter A Message!"
    static let EnterPrice          = "Price Field can't be empty."
    static let PasswordLinkSend    = "Something went wrong, please try again."
    static let InvalidPhoneNumber  = "Invalid phone number"
    static let AcceptTerms         = "Please review and accept Terms & Services."
    static let InvalidPassword     = "Invalid Password"
    static let ValidPassword       = "Please create a password between 6-16 characters with at least any of the following alphabet, number and allowed special characters e.g. !@#$%^&*+=?-"
    static let MatchPassword       = "The passwords you’ve entered don’t match, please review and try again"
    static let inProgress          = "Further work is in progress...."
    static let ImageSize            = "Please select a Image Max 8MB in size."
    static let videoSize            = "Please select a video Max 8MB in size."
    static let DocumentSize         = "Please select a Document Max 8MB in size."
    static let FileAttached         = "File attached sucessfully."
    static let AttachmentAlready    = "There is already an attachement, do you want to replace it?"
    static var btnCancel           = "Cancel"
    static var btnRetry           = "Retry"
    static var btnOk               = "Ok"
    static var btnContinue         = "Continue"
    static var btnDone             = "Done"
    static var btnSelect           = "Please select"
    static var btnTakePhoto        = "Take Photo"
    static var btnSelectPhoto      = "Select Photo"
    static var btnSelectMedia      = "Select Photo/Video"
    static var btnSelectFile       = "Select Document"
}
public enum fileTypes {
    case Image
    case Video
    case File
}
