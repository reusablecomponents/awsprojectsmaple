//
//  ViewController.swift
//  S3Bucket&FCM
//
//  Created by signity on 19/03/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import MobileCoreServices

class ViewController: UIViewController {
    @IBOutlet weak var imageView : UIImageView!
    let imagePicker = UIImagePickerController()
    let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String, kUTTypeHTML as String, kUTTypeText as String, kUTTypeURL as String, kUTTypeSpreadsheet as String, kUTTypePresentation as String, kUTTypeZipArchive as String, kUTTypeMovie as String, kUTTypeImage as String], in: UIDocumentPickerMode.import)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
@IBAction func addImageVideoAttachementAction(_ sender: UIButton) {
    GetCameraPhotoLibModel.shared.getAttachementFile(imagePicker: imagePicker, filePicker: documentPicker, sender: self)
        selectedImageHandler = { Image in
            self.imageView.image = Image
            Image != nil ? _ = self.uploadImageS3(proImage: Image!) : nil
            self.imagePicker.dismiss(animated: true, completion: nil)
        }
    selectedVideoHandler = { file in
        file != nil ? _ = self.uploadVideoS3(proURL: file! as URL) : nil
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    selectedFileHandler = { file in
        file != nil ? _ = self.uploadFileS3(proURL: file! as URL) : nil
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    }
// MARK: -  Upload Image on S3 
func uploadImageS3(proImage:UIImage) -> Bool{
        EventManager.showloader()
        let awsFileUploader = AWSFileUploader()
    awsFileUploader.uploadImageToAWS(image: proImage, fileUrl: nil, type: fileTypes.Image){
            (status,imgPath) in
            if(status)
            {
                print("Uploaded Image Path:")
                print("\(imgPath)")
                //Upload Sucessfull
                
                EventManager.hideloader()
            }
            else
            {
                print("\(imgPath)")
                //Upload Failed
                EventManager.showAlert(alertMessage: ConstantModal.WeakInternet, delegate: self)
                
            }
        }
        return true
    }
// MARK: -  Upload Video on S3 
    func uploadVideoS3(proURL:URL) -> Bool{
        EventManager.showloader()
        let awsFileUploader = AWSFileUploader()
        awsFileUploader.uploadVideoeToAWS(fileUrl: proURL){
            (status,filePath) in
            if(status)
            {
                print("Uploaded Video Path:")
                print("\(filePath)")
                //Upload Sucessfull
                
                EventManager.hideloader()
            }
            else
            {
                print("\(filePath)")
                //Upload Failed
                EventManager.showAlert(alertMessage: ConstantModal.WeakInternet, delegate: self)
                
            }
        }
        return true
    }
    // MARK: -  Upload Video on S3 
    func uploadFileS3(proURL:URL) -> Bool{
        EventManager.showloader()
        let awsFileUploader = AWSFileUploader()
        awsFileUploader.uploadImageToAWS(image: nil, fileUrl: proURL, type: fileTypes.File){
            (status,filePath) in
            if(status)
            {
                print("Uploaded Video Path:")
                print("\(filePath)")
                //Upload Sucessfull
                
                EventManager.hideloader()
            }
            else
            {
                print("\(filePath)")
                //Upload Failed
                EventManager.showAlert(alertMessage: ConstantModal.WeakInternet, delegate: self)
                
            }
        }
        return true
    }
}

